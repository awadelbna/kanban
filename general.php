<?php

require_once("./Models/Model.php");
require_once("./Models/User.php");
require_once("./Models/Task.php");
require_once("./Controllers/Controller.php");
require_once("./Controllers/TasksController.php");
require_once("./Controllers/UsersController.php");
require_once("./Includes/functions.php");

session_start();

$task = new TasksController();
$user = new UsersController();

if (isset($_SESSION["user_id"])) {
    // Show all tasks to specific user
    if (isset($_POST["submit"]) && $_POST["submit"] == "show") {
        $task->getUserTasks();
        exit;

        // Add new task
    } elseif (isset($_POST["submit"]) && $_POST["submit"] == "add") {

        $task->addNewTask($_POST);
        exit;
        // Delete task
    } elseif (isset($_POST["submit"]) && $_POST["submit"] == "delete") {
        $task->deleteTaskById($_POST["task_id"]);
        exit;
    } elseif (isset($_POST["submit"]) && $_POST["submit"] == "update") {

	    $task->editTaskById($_POST);
	    exit;

	} elseif(isset($_POST["logout"])){
        unset($_SESSION["user_id"]);
        session_destroy();
        echo load_view("./Views/signup.html");
        exit;
    }else{
        echo load_view("./Views/index.php");
        exit;
    }

} else {
    // Login
    if (isset($_POST["login"])) {

        if ($user->login($_POST)) {
            echo load_view("./Views/index.php");
            exit;
        } else {
            //show login with errors
        }
        //signup
    } elseif (isset($_POST["signup"])) {
        if ($user->signup($_POST)) {
        	echo load_view("./Views/signup.html");
            exit;
        } else {
            //show signup with errors
        }
    } else {
        echo load_view("./Views/signup.html");
        exit;
    }
}
